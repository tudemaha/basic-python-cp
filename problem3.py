print('Masukkan waktu mulai!')
jam_mulai = int(input('Jam\t: '))
menit_mulai = int(input('Menit\t: '))
detik_mulai = int(input('Detik\t: '))

print('Masukkan waktu selesai!')
jam_selesai = int(input('Jam\t: '))
menit_selesai = int(input('Menit\t: '))
detik_selesai = int(input('Detik\t: '))

jam, menit, detik = 0, 0, 0

if detik_selesai - detik_mulai < 0:
    menit_selesai = menit_selesai - 1
    detik = 60 + detik_selesai - detik_mulai
else:
    detik = detik_selesai - detik_mulai

if menit_selesai - menit_mulai < 0:
    jam_selesai = jam_selesai - 1
    menit = 60 + menit_selesai - menit_mulai
else:
    menit = menit_selesai - menit_mulai

jam = jam_selesai - jam_mulai

print('Tuan Riz berlari selama', end=' ')
if jam != 0: print(f'{jam} jam', end=' ')
if menit != 0: print(f'{menit} menit', end=' ')
if detik != 0: print(f'{detik} detik')
