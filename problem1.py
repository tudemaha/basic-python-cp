dasar_a = int(input('Masukkan harga dasar barang A: '))
jual_a = int(input('Masukkan harga jual barang A: '))
dasar_b = int(input('Masukkan harga dasar barang B: '))
jual_b = int(input('Masukkan harga jual barang B: '))
dasar_c = int(input('Masukkan harga dasar barang C: '))
jual_c = int(input('Masukkan harga jual barang C: '))

A = jual_a - dasar_a
B = jual_b - dasar_b
C = jual_c - dasar_c

keuntungan_max = A
barang = 'A'

if A > keuntungan_max:
    keuntungan_max = A
    barang = 'A'

if B > keuntungan_max:
    keuntungan_max = B
    barang = 'B'

if C > keuntungan_max:
    keuntungan_max = C
    barang = 'C'

print('Barang yang harus ditawarkan adalah barang ' + barang)

# dengan menggunakan perulangan dan percabangan + list dan dictionary
# contoh = [
#     {'var': 'A', 'val': A},
#     {'var': 'B', 'val': B},
#     {'var': 'C', 'val': C}
# ]

# max = contoh[0]

# for i in range(3):
#     if contoh[i]['val'] > max['val']:
#         max = contoh[i]

# print('Barang yang harus ditawarkan adalah barang ' + max['var'])
